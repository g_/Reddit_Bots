from database import Database
import settings

import praw

db = Database()


def get_user_karma(comment):
    # Parses the comment to extract the commenter's karma
    user = comment.author
    if str(user) != comment.submission.author: # Make sure we are not adding the karma of the user who posted the thread
        return user.link_karma+user.comment_karma
