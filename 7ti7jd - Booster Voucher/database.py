import sqlite3
import os


class Database():
    def __init__(self, dbname=os.path.join("database.db")):
        self.dbname = dbname
        self.conn = sqlite3.connect(self.dbname, check_same_thread=False)
        self.conn.text_factory = str

    def connection(self, dbname):
        conn = sqlite3.connect(dbname)
        return conn

    def cursor(self):
        return self.conn.cursor()

    def commit(self):
        self.conn.commit()

    def create_tables(self):
        cursor = self.cursor()
        sql = """
        DROP TABLE IF EXISTS thread;

        CREATE TABLE thread
        (
            thread_id integer PRIMARY KEY AUTOINCREMENT,
            thread_author text,
            thread_karma_sum integer,
            thread_comment_count integer
        )
        """
        cursor.executescript(sql)
        self.commit()

if __name__ == "__main__":
    db = Database()
    db.create_tables()