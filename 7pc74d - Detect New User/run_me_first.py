# Run this script first, since the database starts off empty, we will secretly look though all comments in the subreddit
# And assume every user there is not a new user

# This stops spamming of all the comments if you would run main.py first

# This may take a while
import datetime

import praw

import settings
from database import Database

if __name__ == "__main__":
    db = Database()
    cursor = db.cursor()

    reddit = praw.Reddit(user_agent=settings.REDDIT_USER_AGENT,
                         client_id=settings.REDDIT_CLIENT_ID,
                         client_secret=settings.REDDIT_CLIENT_SECRET,
                         username=settings.REDDIT_USERNAME,
                         password=settings.REDDIT_PASSWORD)

    for comment in reddit.subreddit(settings.REDDIT_SUBREDDIT).comments(limit=1000):
        sqlS = "SELECT * FROM user WHERE user_name = ?"
        cursor.execute(sqlS, (str(comment.author),))
        if cursor.fetchone():
            continue
        else:
            now = datetime.datetime.now()
            sqlN = "INSERT INTO user (user_num_posts, user_date_last_found, user_date_first_found, user_name) VALUES (?, ?, ?, ?)"
            cursor.execute(sqlN, (1, now, now, str(comment.author)))
            print("added new user {}".format(comment.author))
    db.commit()
