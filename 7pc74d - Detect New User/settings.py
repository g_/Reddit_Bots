#Bot account details
REDDIT_USERNAME = ""
REDDIT_PASSWORD = ""
REDDIT_CLIENT_ID = ""
REDDIT_CLIENT_SECRET = ""
REDDIT_USER_AGENT = ""
REDDIT_SUBREDDIT = ""
NEW_USER_REPLY = "Congrats on your first ever post in r/{}!".format(REDDIT_SUBREDDIT)