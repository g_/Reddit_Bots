import datetime

import settings

def parse_comment(db, comment):
    # Reads the comment, if the user doens't exist in the database yet, add them and send a friendly (or unfriendly) reply
    # If they're not new, update the db
    cursor = db.cursor()

    # First search to see if the user already exists
    sqlS = "SELECT * FROM user WHERE user_name = ?"
    cursor.execute(sqlS, (str(comment.author),))
    now = datetime.datetime.now()
    if cursor.fetchone(): # User exists, update record
        sqlU = "UPDATE user SET user_date_last_found = ?, user_num_posts = user_num_posts + 1 WHERE user_name = ?"
        cursor.execute(sqlU, (now, str(comment.author)))
    else: #User doesn't exist yet, create new record
        sqlN = "INSERT INTO user (user_num_posts, user_date_last_found, user_date_first_found, user_name) VALUES (?, ?, ?, ?)"
        cursor.execute(sqlN, (1, now, now, str(comment.author)))
        comment.reply(settings.NEW_USER_REPLY)
