import pip

try:
	import openpyxl
	import praw
except ImportError:
	print("Missing depdencies...")
	print("Installing now...")
	pip.main(["install", "openpyxl"])
	pip.main(["install", "praw"])