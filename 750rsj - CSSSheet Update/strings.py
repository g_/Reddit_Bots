sly_css = """/*SLYTHERIN */
a[href *= '#HPS'] {{height: {}px}}
a[href *= '#HPS']:hover: after
{{content: 'Slytherin: {} points'}}"""

rav_css = """/*RAVENCLAW */
a[href *= '#HPR'] {{height: {}px;}}
a[href *= '#HPR']:hover: after
{{content: 'Ravenclaw: {} points'}}"""

gry_css = """/*GRYFFINDOR */
a[href *= '#HPG'] {{height: {}px;}}
a[href *= '#HPG']:hover: after
{{content: 'Gryffindor: {} points'}}"""

huf_css = """/*HUFFLEPUFF */
a[href *= '#HPH'] {{height: {}px;}}
a[href *= '#HPH']:hover: after
{{content: 'Hufflepuff: {} points'}}"""

css_start = "/*HOUSE TALLY*/"
css_end = "/*END HOUSE TALLY*/"