import requests
import openpyxl
import settings
import strings
import io

def get_sheet(sheetid):
	url = "https://docs.google.com/spreadsheets/d/{}/export?format=xlsx"
	response = requests.get(url.format(sheetid))
	return openpyxl.load_workbook(io.BytesIO(response.content), data_only=True)


def remove_whitespace(list):
	return [line.strip() for line in list]

def get_css_values(wb):
	sheet = wb["Tally"]
	data = []
	data.append(sheet["A2"].value)
	data.append(sheet["A3"].value)
	data.append(sheet["A4"].value)
	data.append(sheet["A5"].value)
	height = [int((x) * settings.REDDIT_CSS_GRAPH_MAX_HEIGHT / (max(data))) for x in data]

	return {"sly":[height[3], data[3]], "rav":[height[2], data[2]], "huf":[height[1], data[1]], "gry":[height[0], data[0]]}

def update_css(reddit, data):

	css = reddit.stylesheet.__call__().stylesheet
	lines = css.split("\n")
	lines = remove_whitespace(lines)
	start_index = lines.index(strings.css_start)
	end_index = lines.index(strings.css_end)

	#Remove everything from before
	del lines[start_index+1:end_index]
	new_start_index = lines.index(strings.css_start)-1

	#Get new data
	sly_css = strings.sly_css.format(data["sly"][0], data["sly"][1])
	huf_css = strings.huf_css.format(data["huf"][0], data["huf"][1])
	rav_css = strings.rav_css.format(data["rav"][0], data["rav"][1])
	gry_css = strings.gry_css.format(data["gry"][0], data["gry"][1])

	lines.insert(new_start_index, sly_css)
	lines.insert(new_start_index, huf_css)
	lines.insert(new_start_index, rav_css)
	lines.insert(new_start_index, gry_css)

	new_css = "\n".join(lines)
	reddit.stylesheet.update(new_css)