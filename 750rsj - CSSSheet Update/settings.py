#Bot account details
REDDIT_USERNAME = ""
REDDIT_PASSWORD = ""
REDDIT_CLIENT_ID = ""
REDDIT_CLIENT_SECRET = ""
REDDIT_USER_AGENT = ""

REDDIT_SUBREDDIT = ""

#Get this from url: https://docs.google.com/spreadsheets/d/1Ulg_fTcXZFCE2_wMYULhFuCVNo09gZbWNOyTGF8WM6s
GSHEET_ID = "1Ulg_fTcXZFCE2_wMYULhFuCVNo09gZbWNOyTGF8WM6s"

REDDIT_CSS_GRAPH_MAX_HEIGHT = 300

BOT_SLEEP = 60