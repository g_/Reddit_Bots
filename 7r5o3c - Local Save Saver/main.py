import re
import webbrowser

import praw
from praw.models.reddit.submission import Submission
from praw.models.reddit.comment import Comment
from jinja2 import Template

import settings

def video_id(value):
    # Retrieved from: https://stackoverflow.com/questions/4356538/how-can-i-extract-video-id-from-youtubes-link-in-python
    return re.search(r"((?<=(v|V)/)|(?<=be/)|(?<=(\?|\&)v=)|(?<=embed/))([\w-]+)", value).group(0)

def get_link_type(link):
    # Checks what type of link it is, we have special conditioning for youtube links

    #Simply checks the last part of the url to see if it has a '.' in it.
    if "." in link.split("/")[-1]:
        return "embed"
    elif "youtube.com" in link or "youtu.be" in link:
        return "youtube"
    else:
        return "link"

def save(item):
    if isinstance(item, Comment):
        return [item.author.name, item.body, "text"]
    elif isinstance(item, Submission):
        if item.selftext:
            return [item.title, item.selftext, "text"]
        else:
            link_type = get_link_type(item.url)
            if link_type == "youtube": # Special condition for youtube/ This is shitty code, I probs should have designed a better more future proof way to implement this, but i cbf
                return [item.title, video_id(item.url), link_type]
            else:
                return [item.title, item.url, link_type]

reddit = praw.Reddit(user_agent=settings.REDDIT_USER_AGENT,
                     client_id=settings.REDDIT_CLIENT_ID,
                     client_secret=settings.REDDIT_CLIENT_SECRET,
                     username=settings.REDDIT_USERNAME,
                     password=settings.REDDIT_PASSWORD)

data = [save(item) for item in reddit.user.me().saved()]
print(data)
template = Template(open("template.html", "r").read())
with open("saved.html", "w") as file:
    file.write(template.render(data=data))

webbrowser.open("file://saved.html")
