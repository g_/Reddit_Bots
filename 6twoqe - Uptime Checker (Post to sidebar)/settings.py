#Bot account details
REDDIT_USERNAME = ""
REDDIT_PASSWORD = ""
REDDIT_CLIENT_ID = ""
REDDIT_CLIENT_SECRET = ""
REDDIT_USER_AGENT = ""

REDDIT_SUBREDDIT = ""

#Whatever your CSS button tags are, change it to these
REDDIT_CSS_TAG_ONLINE = "####"
REDDIT_CSS_TAG_OFFLINE = "######"

SERVER_STATUS_JSON = "http://frontier.ffxiv.com/worldStatus/current_status.json" #Don't change this
SERVER_NAME = "Jenova" #Name of the server you want to track
SERVER_CHECK_PERIOD = 30 #Number of seconds to wait before doing another check

