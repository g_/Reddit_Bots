import settings

def check_server_status(server):
	"""
	Checks whether the given server is online, by checking the official server status
	
	:param server: String, Which server to check
	:return: Boolean, True if server is online, False otherwise
	"""
	import requests
	import json

	response = requests.get(settings.SERVER_STATUS_JSON)
	data = json.loads(response.text.lower())
	if data[server.lower()] == 1:
		return True
	else:
		return False

def toggle_sidebar(subreddit, state):
	"""
	Changes the sidebar of a subreddit to Offline or Online
	
	:param subreddit: A subreddit instance
	:param state: Boolean - The current state of the server: True = Online, False = Offline
	:return: None
	"""
	import re

	description = subreddit.mod.settings()["description"]
	if state:
		updated_button = settings.REDDIT_CSS_TAG_ONLINE
		updated_button += "[{}]({})".format("Online","http://frontier.ffxiv.com/worldStatus/current_status.json")
		print("Changing sidebar status to Online")
	else:
		updated_button = settings.REDDIT_CSS_TAG_OFFLINE
		updated_button += "[{}]({})".format("Offline","http://frontier.ffxiv.com/worldStatus/current_status.json")
		print("Changing sidebar status to Offline")

	regular_expression = "({}|{})(.*)\n".format(settings.REDDIT_CSS_TAG_OFFLINE, settings.REDDIT_CSS_TAG_ONLINE)
	description = re.sub(regular_expression, updated_button, description)
	subreddit.mod.update(description=description)