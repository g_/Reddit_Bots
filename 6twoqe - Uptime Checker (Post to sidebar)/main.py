import praw
import time
import settings
import interface

reddit = praw.Reddit(user_agent=settings.REDDIT_USER_AGENT,
                     client_id=settings.REDDIT_CLIENT_ID,
                     client_secret=settings.REDDIT_CLIENT_SECRET,
                     username=settings.REDDIT_USERNAME,
                     password=settings.REDDIT_PASSWORD)

current_status = interface.check_server_status(settings.SERVER_NAME)
print("Bot is now online")

while True:
	try:
		#Check if the current server status is different to what we currently think it is.
		#If it's different, we should update the sidebar
		new_status = interface.check_server_status(settings.SERVER_NAME)
		if new_status != current_status:
			print("Changing Button")
			current_status = new_status
			interface.toggle_sidebar(reddit.subreddit(settings.REDDIT_SUBREDDIT), new_status)
		time.sleep(30)

	except KeyboardInterrupt:
		break
	except Exception as e:
		print("Error {}".format(e))
		print("Restarting...")
