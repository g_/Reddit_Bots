import settings
import datetime
import queue
import time

def get_when_user_can_post_again(queue):
    #Returns the remaining seconds until a user can post again if they have exceeded their submission cap
    if queue.qsize() >= settings.POST_LIMIT: #only want to check if the user has already exceeded their cap
        last = queue.queue[-1]
        return settings.POST_CHECK_PERIOD - get_submission_age(last).total_seconds()
    else:
        return 0

def get_submission_age(submission):
    #Returns a delta time object from the difference of the current time and the submission creation time
    current_date = datetime.datetime.utcfromtimestamp(time.time())
    submission_date = datetime.datetime.utcfromtimestamp(submission.created_utc)
    return current_date - submission_date

def check_submission(submission):

    check_queue = queue.Queue(maxsize=settings.POST_LIMIT+1)
    #Returns true if the author of the submission has posted something else on the same subreddit within 24 hours
    if submission.author: #Does the author exist? i.e. Deleted account?
        for sub in submission.author.submissions.new(limit=settings.USER_CHECK_LIMIT):
            if sub.subreddit == settings.REDDIT_SUBREDDIT:
                if get_submission_age(sub).total_seconds() < settings.POST_CHECK_PERIOD and sub.banned_by is None:
                    #Collect all posts made within the specified time period, also make sure we only count posts
                    #Which haven't already been removed
                    check_queue.put(sub)
                if check_queue.full():
                    removal_sub = check_queue.get()
                    if removal_sub.banned_by is None: #Check if someone hasn't already removed the submission
                        new_post_time = get_when_user_can_post_again(check_queue)
                        removal_sub.reply(open("reply.txt","r+").read().format(round(new_post_time/3600, 2))).mod.distinguish(sticky=True)
                        removal_sub.mod.remove()
    #This code feels like the worst hack I've done in a long time... sorry future me...