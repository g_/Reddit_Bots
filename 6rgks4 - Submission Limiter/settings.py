#Bot account details
REDDIT_USERNAME = ""
REDDIT_PASSWORD = ""
REDDIT_CLIENT_ID = ""
REDDIT_CLIENT_SECRET = ""
REDDIT_USER_AGENT = ""

REDDIT_SUBREDDIT = ""

#The maximum number of submissions a user has made to check.
#i.e. Bot will by default check the past 50 submissions of a user
USER_CHECK_LIMIT = 50

#Post check period in seconds #1 day = 60*60*24 = 86400
POST_CHECK_PERIOD = 86400

#Maximum number of posts a user can make within the post check period
POST_LIMIT = 5