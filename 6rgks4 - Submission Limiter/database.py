import sqlite3
import os


class Database():
    def __init__(self, dbname=os.path.join("database.db")):
        self.dbname = dbname
        self.conn = sqlite3.connect(self.dbname, check_same_thread=False)
        self.conn.text_factory = str

    def connection(self, dbname):
        conn = sqlite3.connect(dbname)
        return conn

    def cursor(self):
        return self.conn.cursor()

    def commit(self):
        self.conn.commit()

    def create_tables(self):
        cursor = self.cursor()
        sql = """
        DROP TABLE IF EXISTS submission;

        CREATE TABLE submission
        (
            submission_id text,
            submission_date real,
            submission_title text,
            author text
        );
        """
        cursor.executescript(sql)
        self.commit()


if __name__ == "__main__":
    db = Database()
    db.create_tables()