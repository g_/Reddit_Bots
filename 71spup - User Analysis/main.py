"""
An empty bot to be able to start a new bot fast.
Written by /u/SmBe19
Modified by /u/Leftw
"""

import praw
import time
import settings
import logging.handlers
import logging
import interface

# ### USER CONFIGURATION ### #

# The time in seconds the bot should sleep until it checks again.
SLEEP = 60

# ### END USER CONFIGURATION ### #

# ### LOGGING CONFIGURATION ### #
LOG_LEVEL = logging.INFO
LOG_FILENAME = "bot.log"
LOG_FILE_BACKUPCOUNT = 5
LOG_FILE_MAXSIZE = 1024 * 256
# ### END LOGGING CONFIGURATION ### #

# ### LOGGING SETUP ### #
log = logging.getLogger("bot")
log.setLevel(LOG_LEVEL)
log_formatter = logging.Formatter('%(levelname)s: %(message)s')
log_formatter_file = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
log_stderrHandler = logging.StreamHandler()
log_stderrHandler.setFormatter(log_formatter)
log.addHandler(log_stderrHandler)
if LOG_FILENAME is not None:
	log_fileHandler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=LOG_FILE_MAXSIZE, backupCount=LOG_FILE_BACKUPCOUNT)
	log_fileHandler.setFormatter(log_formatter_file)
	log.addHandler(log_fileHandler)
# ### END LOGGING SETUP ### #

def process_mention(comment):
	user = comment.author
	max_age = settings.REDDIT_MAX_AGE * 24 * 60 * 60 # Convert days to seconds
	submission_count = interface.get_submission_count(user, max_age)
	comment_count = interface.get_comment_count(user, max_age)
	most_common = interface.get_most_common_words(user, max_age)
	least_common = interface.get_least_common_words(user, max_age)
	response = settings.REDDIT_REPLY_MESSAGE.format(user, settings.REDDIT_MAX_AGE, comment_count, submission_count, most_common, least_common)
	comment.reply(response)
	comment.mark_read()


# ### MAIN PROCEDURE ### #
def run_bot():
	reddit = praw.Reddit(user_agent=settings.REDDIT_USER_AGENT,
	                     client_id=settings.REDDIT_CLIENT_ID,
						 client_secret=settings.REDDIT_CLIENT_SECRET,
						 username=settings.REDDIT_USERNAME,
	                     password=settings.REDDIT_PASSWORD)

	while True:
		try:
			for mention in reddit.inbox.mentions():
				if not interface.has_replied(mention):
					process_mention(mention)

		# Allows the bot to exit on ^C, all other exceptions are ignored
		except KeyboardInterrupt:
			break
		except Exception as e:
			log.error("Exception %s", e, exc_info=True)
		log.info("sleep for %s s", SLEEP)
		time.sleep(SLEEP)
# ### END MAIN PROCEDURE ### #

# ### START BOT ### #
if __name__ == "__main__":
	if not settings.REDDIT_USER_AGENT:
		log.error("missing useragent")
	else:
		run_bot()
# ### END START BOT ### #