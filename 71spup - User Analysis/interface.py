import settings
from collections import Counter
import time
import re


import settings

def has_replied(submission):
	#Checks if the bot has already looked at a medal, if it hasn't, save it in replied.txt
	try:
		replies = re.findall(r'(\w+)', open("replied.txt", "r+").read().lower())
	except FileNotFoundError:
		with open("replied.txt", "w+") as file:
			replies = re.findall(r"(\w+)", file.read().lower())
	if submission.id in replies or (submission.author == settings.REDDIT_USERNAME):
		return True
	else:
		with open("replied.txt", "a+") as file:
			file.write(submission.__str__()+"\n")
		return False

def get_submission_count(user, age):
	"""
	:param user:  Instance of redditor
	:param age: Maximum age of comment in seconds
	:return: Number of submissions user has created which are less than age seconds old
	"""
	now = time.time()
	return len([x for x in list(user.submissions.new(limit = settings.REDDIT_LISTING_LIMIT)) if x.created_utc > now - age])

def get_comment_count(user, age):
	"""
	:param user:  Instance of redditor
	:param age: Maximum age of comment in seconds
	:return: Number of comments user has made which are less than 'age' seconds old
	"""
	now = time.time()
	return len([x for x in list(user.comments.new(limit = settings.REDDIT_LISTING_LIMIT)) if x.created_utc > now - age])

def get_user_words(user, age):
	"""
	:param user: Instance of redditor
	:param age: Maxiumum age of comment/submission in seconds
	:return: Returns an instance of counter of all words the given user has said.
	"""
	words = Counter()
	now = time.time()
	for comment in [x for x in list(user.comments.new(limit=settings.REDDIT_LISTING_LIMIT)) if
					x.created_utc > now - age]:
		word = comment.body.split(" ")
		words.update(word)

	for submission in [x for x in list(user.submissions.new(limit = settings.REDDIT_LISTING_LIMIT)) if x.created_utc > now - age]:
		word = submission.selftext.split(" ")
		words.update(word)
	return words

def get_most_common_words(user, age):
	#Returns list of most common words as a tuple (word, count)
	data =  get_user_words(user, age).most_common(settings.REDDIT_NUM_COMMON_WORDS)
	string = ""
	for word in data:
		string += re.sub(r"\W", "", word[0]) + ": " + str(word[1]) + ", "
	return string

def get_least_common_words(user, age):
	#Returns list of least common words as a tuple (word, count)
	data = get_user_words(user, age).most_common()[-settings.REDDIT_NUM_LEAST_COMMON_WORDS:]
	string = ""
	for word in data:
		#Escape all special characters
		string += re.sub(r"\W", "",word[0]) + ": " + str(word[1]) + ", " #Too lazy to remove the last comma from the response
	return string

