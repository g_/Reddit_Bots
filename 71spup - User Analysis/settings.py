#Bot account details
REDDIT_USERNAME = ""
REDDIT_PASSWORD = ""
REDDIT_CLIENT_ID = ""
REDDIT_CLIENT_SECRET = ""
REDDIT_USER_AGENT = ""

REDDIT_LISTING_LIMIT = 1000
REDDIT_MAX_AGE = 7 #Maximum number of days to find comments
REDDIT_NUM_COMMON_WORDS = 5
REDDIT_NUM_LEAST_COMMON_WORDS = 5

REDDIT_REPLY_MESSAGE = """
User: {}

Info from the past {} days:


------------


Number of comments: {}

Number of submissions: {}

&nbsp;

Favourite words: {}

Least common words: {}

-----------

Send feedback to [Leftw](https://www.reddit.com/message/compose/?to=tessaract2) | [Source](https://gitlab.com/g_/Reddit_Bots/tree/master/71spup%20-%20User%20Analysis)
"""
