import re
import time
import datetime
import queue

import settings

def get_ban_list(reddit, tags):
	return_list = []
	wiki = reddit.subreddit("UniversalScammerList").wiki["banlist"]
	list = wiki.content_md.split("\n")
	for line in list:
		tag = re.findall(r"(#\w+)", line)
		if elements_in_list(tags, tag):
			name = re.findall(r"/u/(\w+)", line)
			if name:
				return_list.append(name[0])

	return return_list

def elements_in_list(a, b):
	"""
	Returns true if all elements in a are in b
	:param a: A list of strings
	:param b: A list of strings
	:return: Boolean if all elements of a are in b
	"""
	return len(set(a) - set(b)) <= 0

def get_when_user_can_post_again(queue):
	#Returns the remaining seconds until a user can post again if they have exceeded their submission cap
	if queue.qsize() >= settings.POST_LIMIT: #only want to check if the user has already exceeded their cap
		last = queue.queue[-1]
		return settings.POST_CHECK_PERIOD - get_submission_age(last).total_seconds()
	else:
		return 0

def get_submission_age(submission):
	#Returns a delta time object from the difference of the current time and the submission creation time
	current_date = datetime.datetime.utcfromtimestamp(time.time())
	submission_date = datetime.datetime.utcfromtimestamp(submission.created_utc)
	return current_date - submission_date

def check_submission(submission):

	check_queue = queue.Queue(maxsize=settings.POST_LIMIT+1)
	#Returns true if the author of the submission has posted something else on the same subreddit within 24 hours
	if submission.author: #Does the author exist? i.e. Deleted account?
		for sub in submission.author.submissions.new(limit=settings.USER_CHECK_LIMIT):
			if sub.subreddit == settings.REDDIT_SUBREDDIT:
				if get_submission_age(sub).total_seconds() < settings.POST_CHECK_PERIOD and sub.banned_by is None:
					#Collect all posts made within the specified time period, also make sure we only count posts
					#Which haven't already been removed
					check_queue.put(sub)
				if check_queue.full():
					removal_sub = check_queue.get()
					if removal_sub.banned_by is None: #Check if someone hasn't already removed the submission
						new_post_time = get_when_user_can_post_again(check_queue)
						removal_sub.reply(open("reply.txt","r+").read().format(round(new_post_time/60, 2))).mod.distinguish(sticky=True)
						removal_sub.mod.remove()
	#This code feels like the worst hack I've done in a long time... sorry future me...

def is_active(user, min_count, duration):
	#Checks if the given user has made at least x posts in the last "duration" seconds
	"""
	
	:param user: An instance of redditor
	:return: Boolean Truee if active False if not
	"""
	count = 0
	compare_time = time.time() - duration
	for comment in user.comments.new(limit = min_count):
		if comment.created_utc > compare_time:
			count += 1
	for submission in user.submissions.new(limit = min_count):
		if submission.created_utc > compare_time:
			count += 1

	return count >= min_count